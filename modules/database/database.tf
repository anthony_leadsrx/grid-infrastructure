resource "aws_db_subnet_group" "default" {
  name = "main"
  subnet_ids = [ var.data_subnet_a , var.data_subnet_b, var.data_subnet_c ]
  
  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_db_instance" "primary_db" {
  allocated_storage    = 20
  max_allocated_storage = 100
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "foo"
  password             = "foobarbaz"
  parameter_group_name = "default.mysql5.7"
  apply_immediately = true
  skip_final_snapshot = true
  db_subnet_group_name = aws_db_subnet_group.default.name
}