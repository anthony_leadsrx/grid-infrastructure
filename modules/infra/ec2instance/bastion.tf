resource "aws_instance" "bastion" {
    ami = "ami-04590e7389a6e577c"

    instance_type = "t2.micro"

    key_name = "production_key"
    subnet_id = var.public_subnet
    associate_public_ip_address = true

    security_groups = [var.bastion_sg]
}