resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  assign_generated_ipv6_cidr_block = true

  tags = {
    Name = var.vpc_name
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = format("%s %s", var.vpc_name, "Gateway")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table" "rt_public_a" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = format("%s %s", var.vpc_name, "Public A")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table" "rt_public_b" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
   Name = format("%s %s", var.vpc_name, "Public B")
   Owner = var.owner
   Env = var.env
  }
}

resource "aws_route_table" "rt_public_c" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = format("%s %s", var.vpc_name, "Public C")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table_association" "public_a" {
  subnet_id      = aws_subnet.public_a.id
  route_table_id = aws_route_table.rt_public_a.id
}

resource "aws_route_table_association" "public_b" {
  subnet_id      = aws_subnet.public_b.id
  route_table_id = aws_route_table.rt_public_b.id
}

resource "aws_route_table_association" "public_c" {
  subnet_id      = aws_subnet.public_c.id
  route_table_id = aws_route_table.rt_public_c.id
}

resource "aws_subnet" "public_a" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-west-2a"
  tags = {
    Name = format("%s %s", var.vpc_name, "Public A")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "public_b" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-west-2b"
  tags = {
    Name = format("%s %s", var.vpc_name, "Public B")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "public_c" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-west-2c"
  tags = {
    Name = format("%s %s", var.vpc_name, "Public C")
    Owner = var.owner
    Env = var.env
  }
}


resource "aws_route_table" "rt_private_a" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw_a.id
  }

  tags = {
    Name = format("%s %s", var.vpc_name, "Private A")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table" "rt_private_b" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw_b.id
  }

  tags = {
    Name = format("%s %s", var.vpc_name, "Private B")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table" "rt_private_c" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.gw_c.id
  }

  tags = {
    Name = format("%s %s", var.vpc_name, "Private C")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table_association" "private_a" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.rt_private_a.id
}

resource "aws_route_table_association" "private_b" {
  subnet_id      = aws_subnet.private_b.id
  route_table_id = aws_route_table.rt_private_b.id
}

resource "aws_route_table_association" "private_c" {
  subnet_id      = aws_subnet.private_c.id
  route_table_id = aws_route_table.rt_private_c.id
}

resource "aws_route_table" "rt_data_a" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = format("%s %s", var.vpc_name, "Data A")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table" "rt_data_b" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = format("%s %s", var.vpc_name, "Data B")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table" "rt_data_c" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = format("%s %s", var.vpc_name, "Data C")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_route_table_association" "data_a" {
  subnet_id      = aws_subnet.data_a.id
  route_table_id = aws_route_table.rt_data_a.id
}

resource "aws_route_table_association" "data_b" {
  subnet_id      = aws_subnet.data_b.id
  route_table_id = aws_route_table.rt_data_b.id
}

resource "aws_route_table_association" "data_c" {
  subnet_id      = aws_subnet.data_c.id
  route_table_id = aws_route_table.rt_data_c.id
}

resource "aws_subnet" "private_a" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.4.0/24"
  availability_zone = "us-west-2a"
  tags = {
    Name = format("%s %s", var.vpc_name, "Private A")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "private_b" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.5.0/24"
  availability_zone = "us-west-2b"
  tags = {
    Name = format("%s %s", var.vpc_name, "Private B")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "private_c" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.6.0/24"
  availability_zone = "us-west-2c"
  tags = {
    Name = format("%s %s", var.vpc_name, "Private C")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "data_a" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.7.0/24"
  availability_zone = "us-west-2a"
  tags = {
    Name = format("%s %s", var.vpc_name, "Data A")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "data_b" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.8.0/24"
  availability_zone = "us-west-2b"
  tags = {
    Name = "Data B"
    Name = format("%s %s", var.vpc_name, "Data B")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_subnet" "data_c" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.9.0/24"
  availability_zone = "us-west-2c"
  tags = {
    Name = format("%s %s", var.vpc_name, "Data C")
    Owner = var.owner
    Env = var.env
  }
}

resource "aws_eip" "zone_a" {
  vpc = true

  associate_with_private_ip = aws_subnet.public_a.id
}

resource "aws_nat_gateway" "gw_a" {
  allocation_id = aws_eip.zone_a.id
  subnet_id     = aws_subnet.public_a.id
}

resource "aws_eip" "zone_b" {
  vpc = true

  associate_with_private_ip = aws_subnet.public_b.id
}

resource "aws_nat_gateway" "gw_b" {
  allocation_id = aws_eip.zone_b.id
  subnet_id     = aws_subnet.public_b.id
}

resource "aws_eip" "zone_c" {
  vpc = true

  associate_with_private_ip = aws_subnet.public_c.id
}

resource "aws_nat_gateway" "gw_c" {
  allocation_id = aws_eip.zone_c.id
  subnet_id     = aws_subnet.public_c.id
}

resource "aws_security_group" "bastion" {
  name = "allow ssh"
  description = "allow ssh"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 53
    to_port = 53
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 53
    to_port = 53
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks     = ["10.0.0.0/16"]
  }
}

resource "aws_security_group" "grid_lb" {
  name = "allow http/s"
  description = "Allow http/s traffic"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"

    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "grid_instance" {
  name = "allow grid instance traffic"
  description = "Allow http/s traffic"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"

    security_groups     = [aws_security_group.grid_lb.id]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
   security_groups     = [aws_security_group.grid_lb.id]
  }


  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
   security_groups     = [aws_security_group.bastion.id]
  }
  

  egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  egress {
    from_port = 53
    to_port = 53
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 53
    to_port = 53
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 3306
    to_port = 3306
    protocol = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }
}
