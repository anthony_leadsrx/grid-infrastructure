variable "vpc_name" {
    type = string 
}

variable "env" {
    type = string
}

variable "owner" {
    type = string
}