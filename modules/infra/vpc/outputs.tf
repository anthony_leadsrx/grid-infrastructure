output "aws_vpc" {
    value = aws_vpc.main.id
}

output "aws_security_group_bastion" {
    value = aws_security_group.bastion.id
}

output "aws_security_group_grid_lb" {
    value = aws_security_group.grid_lb.id
}

output "aws_security_group_grid_instance" {
    value = aws_security_group.grid_instance.id
}

output "aws_subnet_public_a" {
    value = aws_subnet.public_a.id
}

output "aws_subnet_public_b" {
    value = aws_subnet.public_c.id
}

output "aws_subnet_public_c" {
    value = aws_subnet.public_c.id
}

output "aws_subnet_private_a" {
    value = aws_subnet.private_a.id
}

output "aws_subnet_private_b" {
    value = aws_subnet.private_b.id
}

output "aws_subnet_private_c" {
    value = aws_subnet.private_c.id
}

output "aws_subnet_data_a" {
    value = aws_subnet.data_a.id
}

output "aws_subnet_data_b" {
    value = aws_subnet.data_b.id
}

output "aws_subnet_data_c" {
    value = aws_subnet.data_c.id
}