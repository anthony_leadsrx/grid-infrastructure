variable "vpc_id" {
    type = string
}

variable "lb_sg" {
    type = string
}

variable "grid_instance_sg" {
    type = string
}

variable "public_subnet_a" {
    type = string
}

variable "public_subnet_b" {
    type = string
}

variable "public_subnet_c" {
    type = string
}

variable "private_subnet_a" {
    type = string
}

variable "private_subnet_b" {
    type = string
}

variable "private_subnet_c" {
    type = string
}