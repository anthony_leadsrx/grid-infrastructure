resource "aws_lb" "grid_lb" {
  name = "test-lb"
  internal = false
  load_balancer_type = "application"
  security_groups = [var.lb_sg]
  subnets = [var.public_subnet_a, var.public_subnet_b, var.public_subnet_c]
  idle_timeout = 30
}

resource "aws_lb_target_group" "grid_http" {
    name = "gridv3http"
    port = 80
    protocol = "HTTP"
    vpc_id = var.vpc_id
}

resource "aws_lb_target_group" "grid_https" {
    name = "gridv3https"
    port = 443
    protocol = "HTTPS"
    vpc_id = var.vpc_id
}

resource "aws_lb_listener" "grid_lb_http" {
  load_balancer_arn = aws_lb.grid_lb.arn
  port = 80
  protocol = "HTTP"

  default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.grid_http.arn
  }
}

resource "aws_lb_listener" "grid_lb_https" {
    load_balancer_arn = aws_lb.grid_lb.arn
    port = 443
    protocol = "HTTPS"
    ssl_policy = "ELBSecurityPolicy-2016-08"

    certificate_arn   = "arn:aws:acm:us-west-2:433414402400:certificate/eb21b3b7-cfb3-45ab-9961-46ffd3a76f3a"


    default_action {
      type = "forward"
      target_group_arn = aws_lb_target_group.grid_https.arn
  }
}

resource "aws_launch_configuration" "grid" {
  image_id = "ami-0bc70b71e21514816"
  instance_type = "c5.xlarge"
  iam_instance_profile = "CodeDeployInstanceIAMRole"
  security_groups = [var.grid_instance_sg]
  key_name = "production_key"
}

resource "aws_autoscaling_group" "grid" {
  name = "gridv3dev"
  max_size = 4
  min_size = 2
  health_check_grace_period = 30
  health_check_type = "ELB"
  target_group_arns = [aws_lb_target_group.grid_http.arn, aws_lb_target_group.grid_https.arn]
  launch_configuration = aws_launch_configuration.grid.name
  vpc_zone_identifier = [var.private_subnet_a, var.private_subnet_b, var.private_subnet_c]
}