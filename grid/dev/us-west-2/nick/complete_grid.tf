module "grid_vpc" {
    source = "../../../../modules/infra/vpc"
    vpc_name = "nick-dev"
    env = "dev"
    owner = "nick"
}

module "bastion" {
    source = "../../../../modules/infra/ec2instance"

    public_subnet = module.grid_vpc.aws_subnet_public_a
    bastion_sg = module.grid_vpc.aws_security_group_bastion
}

module "loadbalancer" {
      source = "../../../../modules/infra/loadbalancer"
      vpc_id = module.grid_vpc.aws_vpc
      lb_sg = module.grid_vpc.aws_security_group_grid_lb
      grid_instance_sg = module.grid_vpc.aws_security_group_grid_instance
      public_subnet_a = module.grid_vpc.aws_subnet_public_a
      public_subnet_b = module.grid_vpc.aws_subnet_public_b
      public_subnet_c = module.grid_vpc.aws_subnet_public_c
      private_subnet_a = module.grid_vpc.aws_subnet_private_a
      private_subnet_b = module.grid_vpc.aws_subnet_private_b
      private_subnet_c = module.grid_vpc.aws_subnet_private_c
}

module "database" {
    source = "../../../../modules/database"
    data_subnet_a = module.grid_vpc.aws_subnet_data_a
    data_subnet_b = module.grid_vpc.aws_subnet_data_b
    data_subnet_c = module.grid_vpc.aws_subnet_data_c
}

output "grid_vpc" {
    value = module.grid_vpc.aws_vpc
}

output "aws_security_group_bastion" {
    value = module.grid_vpc.aws_security_group_bastion
}

output "aws_security_group_grid_lb" {
     value = module.grid_vpc.aws_security_group_grid_lb
}

output "aws_security_group_grid_instance" {
    value = module.grid_vpc.aws_security_group_grid_instance
}

output "aws_subnet_public_a" {
    value = module.grid_vpc.aws_subnet_public_a
}

output "aws_subnet_public_b" {
    value = module.grid_vpc.aws_subnet_public_b
}

output "aws_subnet_public_c" {
    value = module.grid_vpc.aws_subnet_public_c
}

output "aws_subnet_private_a" {
    value = module.grid_vpc.aws_subnet_private_a
}

output "aws_subnet_private_b" {
    value = module.grid_vpc.aws_subnet_private_b
}

output "aws_subnet_private_c" {
    value = module.grid_vpc.aws_subnet_private_c
}

output "aws_subnet_data_a" {
    value = module.grid_vpc.aws_subnet_data_a
}

output "aws_subnet_data_b" {
    value = module.grid_vpc.aws_subnet_data_b
}

output "aws_subnet_data_c" {
    value = module.grid_vpc.aws_subnet_data_c
}